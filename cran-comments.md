Dear CRAN Team,
this is a resubmission of package 'maSAE'. I have added the following changes:

* FIXME

Please upload to CRAN.
Best, Andreas Dominik

# Package maSAE 2.0.3.9000

Reporting is done by packager version 1.11.1


## Test environments
- R Under development (unstable) (2021-03-29 r80130)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 3 (beowulf)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)

## Local test results
- RUnit:
    maSAE_unit_test - 25 test functions, 0 errors, 0 failures in 69 checks.
- Testthat:
    
- Coverage by covr:
    maSAE Coverage: 88.51%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for predict_v1 by 40.
     Exceeding maximum cyclomatic complexity of 10 for predict_v2 by 20.
     Exceeding maximum cyclomatic complexity of 10 for pred_synth by 3.
- lintr:
    found 157 lints in 2040 lines of code (a ratio of 0.077).
- codetools::checkUsagePackage:
    found 19 issues.
- devtools::spell_check:
    found 27 unkown words.

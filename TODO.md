
- Add global prediction?
  * This works:
    reg2p_nex <- twophase(formula=tvol ~ mean + stddev + max +q75,
                        data=grisons,
                        phase_id=list(phase.col = "phase_id_2p", terrgrid.id =2),
                        boundary_weights = "boundary_weights")
  * This doesn't:
    global <- s12
    global$smallarea <- "global"
    maSAE <- predict(saObj(data = global, f = update(formula.s1, ~ . | smallarea),
                         s2 = 's2', auxiliary_weights = "boundary_weights"), 
                   all = TRUE)
